#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib.syndication.views import Feed

from .models import Post


class AllPostsRssFeed(Feed):
    title = "Blog Demo"

    link = "/"

    description = "Demo"

    def items(self):
        return Post.objects.all()

    def item_title(self, item):
        return '[%s] %s' % (item.category, item.title.encode("utf-8"))

    def item_description(self, item):
        return item.body
